# Star-CorrelatedK

This C library loads the radiative properties of a gas mixture saved in
sck file format. See `sck.5` for the format specification.

## Requirements

- C compiler
- POSIX make
- pkg-config
- [RSys](https://gitlab.com/vaplv/rsys)
- [mandoc](https://mandoc.bsd.lv)

## Installation

Edit config.mk as needed, then run:

    make clean install

## Release notes

### Version 0.1

- Write the man page directly in mdoc's roff macros, instead of using
  the intermediate scdoc source.
- Replace CMake by Makefile as build system.
- Update compiler and linker flags to increase the security and
  robustness of generated binaries.
- Provide a pkg-config file to link the library as an external
  dependency.

## License

Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)  
Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique (CNRS)

Star-CK is free software released under the GPL v3+ license: GNU GPL
version 3 or later. You are welcome to redistribute it under certain
conditions; refer to the COPYING file for details.
