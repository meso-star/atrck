/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SCK_H
#define SCK_H

#include <rsys/hash.h>
#include <rsys/rsys.h>

/* Library symbol management */
#if defined(SCK_SHARED_BUILD) /* Build shared library */
  #define SCK_API extern EXPORT_SYM
#elif defined(SCK_STATIC) /* Use/build static library */
  #define SCK_API extern LOCAL_SYM
#else /* Use shared library */
  #define SCK_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the sck function `Func'
 * returns an error. One should use this macro on sck function calls for which
 * no explicit error checking is performed */
#ifndef NDEBUG
  #define SCK(Func) ASSERT(sck_ ## Func == RES_OK)
#else
  #define SCK(Func) sck_ ## Func
#endif

/* Forward declaration of external data types */
struct logger;
struct mem_allocator;

struct sck_create_args {
  struct logger* logger; /* May be NULL <=> default logger */
  struct mem_allocator* allocator; /* NULL <=> use default allocator */
  int verbose; /* Verbosity level */
};
#define SCK_CREATE_ARGS_DEFAULT__ {NULL, NULL, 0}
static const struct sck_create_args SCK_CREATE_ARGS_DEFAULT =
  SCK_CREATE_ARGS_DEFAULT__;

struct sck_load_args {
  const char* path;
  int memory_mapping; /* Use memory mapping instead of normal loading */
};
#define SCK_LOAD_ARGS_NULL__ {NULL, 0}
static const struct sck_load_args SCK_LOAD_ARGS_NULL = SCK_LOAD_ARGS_NULL__;

struct sck_load_stream_args {
  FILE* stream;
  const char* name; /* Stream name */
  /* Use memory mapping instead of normal loading. Note that memory mapping
   * cannot be used on some stream like stdin */
  int memory_mapping;
};
#define SCK_LOAD_STREAM_ARGS_NULL__ {NULL, "stream", 0}
static const struct sck_load_stream_args SCK_LOAD_STREAM_ARGS_NULL =
  SCK_LOAD_STREAM_ARGS_NULL__;

struct sck_band {
  double lower; /* Lower band wavelength in nm (inclusive) */
  double upper; /* Upper band wavelength in nm (exclusive) */
  size_t quad_pts_count; /* #quadrature points */
  size_t id;

  float* ks_list; /* Per node ks */

  /* Internal data */
  const struct sck* sck__;
  const void* band__;
};
#define SCK_BAND_NULL__ {0, 0, 0, 0, NULL, NULL, NULL}
static const struct sck_band SCK_BAND_NULL = SCK_BAND_NULL__;

struct sck_quad_pt {
  float* ka_list; /* Per node ka */
  double weight;
  size_t id;
};
#define SCK_QUAD_PT_NULL__ {NULL, 0, 0}
static const struct sck_quad_pt SCK_QUAD_PT_NULL = SCK_QUAD_PT_NULL__;

/* Forward declaration of opaque data types */
struct sck;

BEGIN_DECLS

/*******************************************************************************
 * AtrKC API
 ******************************************************************************/
SCK_API res_T
sck_create
  (const struct sck_create_args* args,
   struct sck** sck);

SCK_API res_T
sck_ref_get
  (struct sck* sck);

SCK_API res_T
sck_ref_put
  (struct sck* sck);

SCK_API res_T
sck_load
  (struct sck* sck,
   const struct sck_load_args* args);

SCK_API res_T
sck_load_stream
  (struct sck* sck,
   const struct sck_load_stream_args* args);

/* Validates radiative coefficients. Data checks have already been carried out
 * during loading, notably on spectral bands and quadrature points, but this
 * function performs longer and more thorough tests. It reviews all scattering
 * and absorption coefficients to check their validity, i.e. whether they are
 * positive or zero. Note that checking radiative coefficients is not mandatory,
 * in order to speed up the loading step and avoid loading/unloading them when
 * using memory mapping. */
SCK_API res_T
sck_validate
  (const struct sck* sck);

SCK_API size_t
sck_get_bands_count
  (const struct sck* sck);

SCK_API size_t
sck_get_nodes_count
  (const struct sck* sck);

SCK_API res_T
sck_get_band
  (const struct sck* sck,
   const size_t iband,
   struct sck_band* band);

/* Returns the range of band indices covered by a given spectral range. The
 * returned index range is degenerated (i.e. ibands[0] > ibands[1]) if no band
 * is found */
SCK_API res_T
sck_find_bands
  (const struct sck* sck,
   const double range[2], /* In nm. Limits are inclusive */
   size_t ibands[2]); /* Range of overlaped bands. Limits are inclusive */

SCK_API res_T
sck_band_get_quad_pt
  (const struct sck_band* band,
   const size_t iquad_pt,
   struct sck_quad_pt* quad_pt);

SCK_API res_T
sck_band_sample_quad_pt
  (const struct sck_band* band,
   const double r, /* Canonical random number in [0, 1[ */
   size_t* iquad_pt); /* Index of the sampled quadrature point */

SCK_API res_T
sck_quad_pt_compute_hash
  (const struct sck_band* band,
   const size_t iquad_pt,
   hash256_T hash);

SCK_API res_T
sck_band_compute_hash
  (const struct sck* sck,
   const size_t iband,
   hash256_T hash);

SCK_API res_T
sck_compute_hash
  (const struct sck* sck,
   hash256_T hash);

/* Returns the path of the file or the name of the stream from which the data
 * was loaded */
SCK_API const char*
sck_get_name
  (const struct sck* sck);

END_DECLS

#endif /* SCK_H */
