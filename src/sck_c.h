/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SCK_C_H
#define SCK_C_H

#include <rsys/dynamic_array_double.h>
#include <rsys/logger.h>
#include <rsys/ref_count.h>
#include <rsys/str.h>

struct mem_allocator;

struct quad_pt {
  struct band* band; /* Band to which the quadrature point belongs */
  float* ka_list; /* Per node ka */
  size_t map_len;
  double weight;
};

static INLINE void
quad_pt_init(struct mem_allocator* allocator, struct quad_pt* quad)
{
  ASSERT(quad);
  (void)allocator;
  quad->band = NULL;
  quad->ka_list = NULL;
  quad->map_len = 0;
  quad->weight = 0;
}

extern LOCAL_SYM void
quad_pt_release
  (struct quad_pt* quad);

extern LOCAL_SYM res_T
quad_pt_copy
  (struct quad_pt* dst,
   const struct quad_pt* src);

static INLINE res_T
quad_pt_copy_and_release(struct quad_pt* dst, struct quad_pt* src)
{
  ASSERT(dst && src);
  dst->band = src->band;
  dst->ka_list = src->ka_list;
  dst->map_len = src->map_len;
  dst->weight = src->weight;
  return RES_OK;
}

/* Define the dynamic array of quadrature points */
#define DARRAY_NAME quad_pt
#define DARRAY_DATA struct quad_pt
#define DARRAY_FUNCTOR_INIT quad_pt_init
#define DARRAY_FUNCTOR_RELEASE quad_pt_release
#define DARRAY_FUNCTOR_COPY quad_pt_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE quad_pt_copy_and_release
#include <rsys/dynamic_array.h>

struct band {
  struct sck* sck;
  double low; /* Lower bound in nm (inclusive) */
  double upp; /* Upper bound in nm (exclusive) */
  size_t map_len; /* 0 <=> band's data are not mapped, i.e. they are loaded */
  float* ks_list; /* Per node ks */
  struct darray_quad_pt quad_pts;
  struct darray_double quad_pts_cumul;
};

static INLINE void
band_init(struct mem_allocator* allocator, struct band* band)
{
  ASSERT(band);
  band->sck = NULL;
  band->low = DBL_MAX;
  band->upp =-DBL_MAX;
  band->map_len = 0;
  band->ks_list = NULL;
  darray_quad_pt_init(allocator, &band->quad_pts);
  darray_double_init(allocator, &band->quad_pts_cumul);
}

extern LOCAL_SYM void
band_release
  (struct band* band);

extern LOCAL_SYM res_T
band_copy
  (struct band* dst,
   const struct band* src);

static INLINE res_T
band_copy_and_release(struct band* dst, struct band* src)
{
  res_T res = RES_OK;
  ASSERT(dst && src);

  dst->sck = src->sck;
  dst->low = src->low;
  dst->upp = src->upp;
  dst->map_len = src->map_len;
  dst->ks_list = src->ks_list;
  res = darray_quad_pt_copy_and_release(&dst->quad_pts, &src->quad_pts);
  if(res != RES_OK) return res;
  res = darray_double_copy_and_release
    (&dst->quad_pts_cumul, &src->quad_pts_cumul);
  if(res != RES_OK) return res;
  return RES_OK;
}

/* Define the dynamic array of bands */
#define DARRAY_NAME band
#define DARRAY_DATA struct band
#define DARRAY_FUNCTOR_INIT band_init
#define DARRAY_FUNCTOR_RELEASE band_release
#define DARRAY_FUNCTOR_COPY band_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE band_copy_and_release
#include <rsys/dynamic_array.h>

struct mem_allocator;

struct sck {
  /* Loaded data */
  uint64_t pagesize;
  uint64_t nnodes;
  struct darray_band bands;

  size_t pagesize_os;
  struct str name; /* path/stream name */

  struct mem_allocator* allocator;
  struct logger* logger;
  struct logger logger__; /* Default logger */
  int verbose;
  ref_T ref;
};

#endif /* SCK_C_H */
